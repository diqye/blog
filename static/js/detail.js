(function(exports){

  //语法高亮
  hljs.configure({
    tabReplace: '    ', // 4 spaces
    classPrefix: 'lang'     // don't append class prefix
                      // … other options aren't changed
  })
  hljs.initHighlighting();


  new Clipboard('[copy-url]',{
    text:function(trigger){
      return window.location.href;
    }
  }).on("success",function(){
    tip("已复制本页URL,欢迎分享到QQ,微信等")
  });

  //手机浏览器 处理
  // userAgent -> Boolean
  function isM(agent){
    return agent.indexOf("Mobile") != -1;
  }
  // userAgent -> Boolean
  function isWX(agent){
    return agent.indexOf("MicroMessenger") != -1;
  }

  // String -> ()
  function tip(msg){
    var body = document.querySelector("body")
    var div = document.createElement("div")
    div.className = "tip"
    div.innerHTML = msg
    body.appendChild(div)
    setTimeout(function(){
      div.remove();
    },3000);
  }
  // navigator.userAgent


}(this))
