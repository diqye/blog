# myppt
基于markdown语法,在浏览器上播放的ppt

## [https://git.oschina.net/diqye/shar](https://git.oschina.net/diqye/shar)
在线预览[http://www.diqye.com/mpt-shar.html](http://www.diqye.com/mpt-shar.html)

## 特点
- markdown语法
- 浏览器上播放
- 语法高亮
- 自适应
- 空格，`c-n` 下一页  `c-p`,`Meta` 为上一页
- 更关注于内容本身而不是如何排版

## 使用
```html
<xmp>
  something...
</xmp> 
<script src="../lib/impt.js" main="./app.js"></script>
```
也可以(无需clone直接保存为html去使用)
```html
<xmp>
  something...
</xmp> 
<script src="https://git.oschina.net/diqye/shar/raw/master/lib/impt.js" main="https://git.oschina.net/diqye/shar/raw/master/demo/oscapp.js"></script>
```

## 分屏
- 默认空行分屏
- 也可以使用`:start` 和`:end`进行分屏
  - 例如...

```
something...
(:start 去掉前面的括号
something...
:end
```



## demo
1. `index.html`是一个完整的demo
2. 本readme本身就是一个mppt [http://sandbox.runjs.cn/show/c58ijvkq](http://sandbox.runjs.cn/show/c58ijvkq)
