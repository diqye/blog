CREATE DATABASE IF NOT EXISTS diqye_blog DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
use diqye_blog;
CREATE TABLE IF NOT EXISTS blog_t(
   pathid VARCHAR(50)  NOT NULL,
   title VARCHAR(100) NOT NULL,
   banner VARCHAR(100),
   content TEXT,
   c_date VARCHAR(20),
   is_del INT DEFAULT 0,
   tag_1 VARCHAR(25),
   tag_2 VARCHAR(25),
   tag_3 VARCHAR(25),
   PRIMARY KEY (pathid)
);

CREATE TABLE IF NOT EXISTS sswd_t(
   roomid INT NOT NULL auto_increment,
   dicts VARCHAR(100) NOT NULL COMMENT "平民词,卧底词",
   total INT NOT NULL COMMENT "一共多少个人数",
   cnum INT NOT NULL COMMENT "已进入房间的人数",
   wd   VARCHAR(50) NOT NULL COMMENT "卧底是几号 多个以,隔开",
   PRIMARY KEY (roomid)
);
