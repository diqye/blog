var mysql = require("mysql")


// SQLString -> (Error -> Rows -> Fields) -> ()
function esql(sql,fn){
  var connection = mysql.createConnection({
    host     : process.env.M_HOST || 'localhost',
    user     : process.env.M_USER || "root",
    port     : process.env.M_PORT || "3306",
    password : process.env.M_PASSWORD || '',
    database : process.env.M_DB || 'diqye_blog'
  });
  connection.connect();
  connection.query(sql,fn);
  connection.end();
}
// SQL String -> [a] -> (Error -> Rows -> Fields) -> ()
function esqlp(sql,params,fn){
  var connection = mysql.createConnection({
    host     : process.env.M_HOST || 'localhost',
    user     : process.env.M_USER || "root",
    port     : process.env.M_PORT || "3306",
    password : process.env.M_PASSWORD || '',
    database : process.env.M_DB || 'diqye_blog'
  });
  connection.connect();
  connection.query(sql,params,fn);
  connection.end();
}

exports.esql = esql
exports.esqlp = esqlp
